extends KinematicBody2D

const SPEED = 200

onready var PointNode = $Point
onready var HandNode = $Hand
onready var start_position = HandNode.position
onready var on_hand
var holding = false
var score:int = 0

onready var SFXHandDescendNode = $SFX_Hand_Descend
onready var SFXHandAscendNode = $SFX_Hand_Ascend
onready var SFXFall = [$SFX_Fall1, $SFX_Fall2]

#pickup
onready var TweenGrabStartNode = $TweenGrabStart
onready var TweenGrabEndNode = $TweenGrabEnd

#drop
onready var TweenDropStartNode = $TweenDropStart
onready var TweenDropEndNode = $TweenDropEnd

enum PLAYERS {
	Player1,
 	Player2,
	Player3,
	Player4
	}


var players : Dictionary = { 

	PLAYERS.Player4:{
		"Input":{
			"UP":"P4_Move_Up",
			"DOWN":"P4_Move_Down",
			"LEFT":"P4_Move_Left",
			"RIGHT":"P4_Move_Right",
			"ACTION":"P4_Catch"
		}
	},
	
	PLAYERS.Player3:{
		"Input":{
			"UP":    "P3_Move_Up",
			"DOWN":  "P3_Move_Down",
			"LEFT":  "P3_Move_Left",
			"RIGHT": "P3_Move_Right",
			"ACTION":"P3_Catch"
		}
	},
		PLAYERS.Player2:{
		"Input":{
			"UP":"P2_Move_Up",
			"DOWN":"P2_Move_Down",
			"LEFT":"P2_Move_Left",
			"RIGHT":"P2_Move_Right",
			"ACTION":"P2_Catch"
		}
	},
	
	PLAYERS.Player1:{
		"Input":{
			"UP":"P1_Move_Up",
			"DOWN":"P1_Move_Down",
			"LEFT":"P1_Move_Left",
			"RIGHT":"P1_Move_Right",
			"ACTION":"P1_Catch"
		}
	}
	}

func _ready():
	randomize()


func _process(_delta):
	
	if holding:
		get_node("picked").show()
	else:
		get_node("picked").hide()
		

	movement()
	
	if on_hand:
		if holding:
			on_hand.global_position = self.get_node("Hand").global_position

	else:
		pass

func _input(_event):
	if Input.is_action_pressed(players[get_parent().player].Input.ACTION):
		if holding:
			start_drop_descend_animation()

		elif not holding:
			start_pickup_descend_animation()



func movement():
	var right = int(Input.is_action_pressed(players[get_parent().player].Input.RIGHT))
	var left = int (Input.is_action_pressed(players[get_parent().player].Input.LEFT))
	var up = int(Input.is_action_pressed(players[get_parent().player].Input.UP))
	var down = int(Input.is_action_pressed(players[get_parent().player].Input.DOWN))
	
	if right == 1:
		get_node("Hand").flip_h = false
	if left == 1:
		get_node("Hand").flip_h = true

	var movement = Vector2(right - left, down - up).normalized()
	var _coll = move_and_slide(movement * SPEED)


func start_pickup_descend_animation(): #1 start descend
	SFXHandDescendNode.play()
	self.TweenGrabStartNode.interpolate_property(HandNode, "position", HandNode.position, PointNode.position, .7, Tween.TRANS_BACK, Tween.EASE_IN)
	self.TweenGrabStartNode.start()


func end_pickup_descend_animation(_object, _key): #2 end descend
	print(get_node("Body/CollisionShape2D").disabled)
	call_deferred("start_pickup_ascend_animation")


func start_pickup_ascend_animation(): #3 start ascend
	SFXHandAscendNode.play()
	get_node("Body/CollisionShape2D").disabled = false
	self.TweenGrabEndNode.interpolate_property(HandNode, "position", HandNode.position, start_position, .6, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	self.TweenGrabEndNode.start()
	print(get_node("Body/CollisionShape2D").disabled)


func end_pickup_ascend(_object, _key): #4 end ascend
	get_node("Body/CollisionShape2D").disabled = true


func start_drop_descend_animation(): #1 start descend
	SFXHandDescendNode.play()
	self.TweenDropStartNode.interpolate_property(HandNode, "position", HandNode.position, PointNode.position, .7, Tween.TRANS_BACK, Tween.EASE_IN)
	self.TweenDropStartNode.start()
	pass


func end_drop_descend_animation(_object, _key): #2 end descend
	if holding == true:
		if self.global_position.x < 241 or self.global_position.x > 553:
			score += on_hand.pontos
			on_hand._set_pontos()
			#score += 1
			var r = randi() % 2
			var random_pitch = rand_range(0.8, 2)
			SFXFall[r].pitch_scale = random_pitch
			SFXFall[r].play()

	call_deferred("start_drop_ascend_animation")
	pass


func start_drop_ascend_animation(): #3 start ascend
	SFXHandAscendNode.play()
	self.TweenDropEndNode.interpolate_property(HandNode, "position", HandNode.position, start_position, .6, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	self.TweenDropEndNode.start()
	holding = false
	pass


func end_drop_ascend_animation(_object, _key): #4 end ascend

	pass


func person_pickup(area):
	print("teste")
	if !holding:
		on_hand = area.get_parent()
		holding = true
