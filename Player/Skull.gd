extends Node2D

onready var HandNode = $Hand

export (int, "Player1", "Player2", "Player3", "Player4") onready var player
onready var score = 0
onready var scoreText = ""
onready var ScoreLabel = $PointsLabel
func _ready():
	
	pass

func _process(_delta):
	score = get_node("Hand").score
	scoreText = "Player" + str(player+1) +": " + str(score) + "$"
	ScoreLabel.text = scoreText
	if HandNode.global_position.x < self.global_position.x:
		self.get_node("Skull").flip_h = true
		self.get_node("Hat").flip_h = true
	elif HandNode.global_position.x > self.global_position.x:
		self.get_node("Skull").flip_h = false
		self.get_node("Hat").flip_h = false
