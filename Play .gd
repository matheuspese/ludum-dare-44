extends Button

onready var map1 = preload("res://Map.tscn")
onready var map2 = preload("res://Map2.tscn")
onready var map
onready var OptionButtonNode = $OptionButton


func _ready():
	if OptionButtonNode.get_selected_id() == 0:
		map = map1
	if OptionButtonNode.get_selected_id() == 1:
		map = map2


func _on_Play__pressed():
	if OptionButtonNode.get_selected_id() == 0:
		map = map1
	if OptionButtonNode.get_selected_id() == 1:
		map = map2

	var instanced_map = map.instance()
	get_parent().get_parent().add_child(instanced_map)
	
	call_deferred("self_destruct")
	

func self_destruct():
	self.get_parent().queue_free()